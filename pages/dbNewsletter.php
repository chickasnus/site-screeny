<?php

    try {
        $db = new PDO('sqlite:../database.db');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch (Exception $e) {
        echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
        die();
    }

    $email = $_POST['email'];
   

    $query = $db->query("INSERT INTO newsletter (email) VALUES('$email') ");

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Merci de nous suivre</title>
</head>
    <body>
        <p>Merci de votre inscription à la newsletter. <br>Vous allez être redirigé vers l'accueil dans 5 secondes.</p>
        <script src="../ressources/js/redirectValid.js"></script>
    </body>
</html>


