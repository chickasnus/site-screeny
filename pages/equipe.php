<?php

    try {
        $db = new PDO('sqlite:../database.db');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch (Exception $e) {
        echo "Impossible d'accéder à la base de données SQLite : ".$e->getMessage();
        die();
    }

    $pdoState =  $db->query('SELECT * FROM user WHERE user.foundator = 1');
    $result = $pdoState->fetchAll();

?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- ne pas oublier de modifier cette balise en dessous -->
        <title>A propos de nous</title>
        <!-- lien pour la police de la nav -->
        <link href="https://fonts.googleapis.com/css?family=Gelasio|Righteous&display=swap" rel="stylesheet">
        <!-- lien pour la police du body -->
        <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville&display=swap" rel="stylesheet">
        <!-- ne pas supprimer cet balise link pour le style de la nav et du footer -->
        <link rel="stylesheet" href="../ressources/css/header.css">
        <link rel="stylesheet" href="../ressources/css/footer.css">
        <!-- style commun du body, ne pas supprimer -->
        <link rel="stylesheet" href="../ressources/css/commun.css">
        <!-- ajouter votre css à la suite --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
        
        <!--[if lte IE 8]><!-->
        <!--<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/grids-responsive-old-ie-min.css">
        [endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/grids-responsive-min.css">
        <!--<![endif]-->
        <link rel="stylesheet" href="../ressources/css/equipe.css">


    </head>
    <body>
        <!-- inclusion de header avec la navigation -->
        <?php 
            include("./header.php")
        ?>

        <main>
            <section>
                
                <div class="banner">
                    <div class="banner-team">
                        <h1 id="js-title-header" class="banner-head">
                            Découvrez notre équipe.
                        </h1>
                    </div>
                </div>
                <article class="apropos">
                    <h1>A propos de nous</h1>
                    <p>
                        Beatae sed laboriosam saepe in cum cumque soluta est ut. Laboriosam illo voluptatem sapiente qui exercitationem. Reprehenderit aspernatur est. Incidunt commodi qui mollitia.
    
                        Maiores praesentium inventore quis molestiae molestiae non perferendis. Perferendis at quis cumque amet voluptate. Quam ad quam tempora voluptatibus.

                        Provident omnis quo in quis dignissimos eos et. Vitae ad quasi aut placeat commodi. Velit adipisci optio nisi exercitationem. Aut nam aperiam voluptas ut autem. Fugiat inventore est et accusantium nihil. Perferendis quasi enim vero officiis voluptatem aliquid sunt.
                    </p>
                </article>

            </section>
            <section>
                <hgroup class="group-title">
                    <h2>L'équipe</h2>
                    <h3>Les fondateurs</h3>
                </hgroup>
                <div class="pure-g">

                <?php foreach($result as $value): ?>

                    <article class="pure-u-1 pure-u-md-1-4">
                        <div class="l-box">
                            <img class="pure-img" src="<?= $value['url_img'] ?>" alt="<?= $value['alt_img'] ?>">
                            <h4><?= $value['last_name'].' '.$value['first_name'] ?></h4>
                            <h5><?= $value['profession'] ?></h5>
                            <p>Quidem ipsam omnis id quasi neque iste.</p>
                        </div>
                    </article> 

                <?php endforeach ?>

                </div>
            </section>
            <button class="bouton-up hidden" id="js-prosition-scroll">
        <img 
            src="../ressources/images/angle-up-solid.svg" 
            alt="un triangle aux trois côtés égaux"
            height="50px"
            width="50px" />
        </button>
        </main>


        <?php
            include("./footer.php")
        ?>
    <script src="../ressources/js/header-menu.js"></script>
    <script src="../ressources/js/animeTitleHeader_equipe.js"></script> 
    <script src="../ressources/js/returnButtonScroll.js"></script> 
    </body>
</html>