CREATE TABLE user
(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    last_name VARCHAR(100) NOT NULL,
    first_name VARCHAR(100) NOT NULL,
    email VARCHAR(255) NOT NULL,
    foundator BOOLEAN NOT NULL,
    url_img VARCHAR(255),
    alt_img VARCHAR(255),
    profession VARCHAR(255)
);
INSERT INTO user VALUES(1,'Bechtelar','Axel','axel.bechtelar@screeny.com',1,'https://via.placeholder.com/320x240.png','image png exemple de 320 sur 240','developpeur web');

INSERT INTO user VALUES(2,'Farrell','Colten','colten.farrell@screeny.com',1,'https://via.placeholder.com/320x240.png','image png exemple de 320 sur 240','developpeur web front-end');

INSERT INTO user VALUES(3,'Rutherford','Katelin','katelin.rutherford@screeny.com',1,'https://via.placeholder.com/320x240.png','image png exemple de 320 sur 240','developpeur web back-end');

INSERT INTO user VALUES(4,'Conn','Berry','berry.conn@screeny.com',1,'https://via.placeholder.com/320x240.png','image png exemple de 320 sur 240','lead developpeur');

CREATE TABLE contact (id INTEGER PRIMARY KEY AUTOINCREMENT ,last_name VARCHAR(100) NOT NULL, email VARCHAR(255) NOT NULL, message VARCHAR(255) NOT NULL);

CREATE TABLE newsletter (id INTEGER PRIMARY KEY AUTOINCREMENT ,email VARCHAR(255) NOT NULL);
