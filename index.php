<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css?family=Gelasio|Raleway|Sigmar+One&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./ressources/css/index.css">
    <link rel="stylesheet" href="./ressources/css/header.css">
    <link rel="stylesheet" href="./ressources/css/footer.css">
    <link rel="stylesheet" href="./ressources/css/commun.css">
    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
        
        <!--[if lte IE 8]><!-->
        <!--<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/grids-responsive-old-ie-min.css">
        [endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/grids-responsive-min.css">
</head>
<body>
        <?php 
            include("./pages/header.php");
        
        ?>

    <main class = "mainProduit">
        

        <h1 class="upperCase grandTitre">screeny</h1>

        <div class="leGrosConteneur">
            <button id="boutonG" onclick = "boutonDroite()">G</button>
            <div id="imageContainer">
                <!--<button class = "boutonGauche">gauche
                </button> -->
                <div class = "blocImgText">
                    <img class = "image" src="./ressources/images/ecran1.webp" alt="écran de face" />
                    <p class = "description">Habere non (sed esse unus beate non implicari non pro non quas quos
                     amicitias beate non rerum non Graecia implicari beate esse sibi securitatem vivendum quos esse
                      si beate vivendum nimis ne esse cuique pro quos cuique habere pro adducas tamquam esse argutiis):
                       remittas non velis tamquam mirabilia sapientes cum.</p>
                </div>

                <div class = "blocImgText">
                    <img class = "image" src="./ressources/images/ecranBiais.webp" alt="écran de biais" />
                    <p class = "description">Habere non (sed esse unus beate non implicari non pro non quas quos
                     amicitias beate non rerum non Graecia implicari beate esse sibi securitatem vivendum quos esse
                      si beate vivendum nimis ne esse cuique pro quos cuique habere pro adducas tamquam esse argutiis):
                       remittas non velis tamquam mirabilia sapientes cum.</p>
            
                </div>

                <div class = "blocImgText">
                    <img class = "image" src="./ressources/images/ecranCote.webp" alt="écran de côté" />
                    <p class = "description">Habere non (sed esse unus beate non implicari non pro non quas quos
                     amicitias beate non rerum non Graecia implicari beate esse sibi securitatem vivendum quos esse
                      si beate vivendum nimis ne esse cuique pro quos cuique habere pro adducas tamquam esse argutiis):
                       remittas non velis tamquam mirabilia sapientes cum.</p>
            
                </div>

                <div class = "blocImgText">
                    <img class = "image" src="./ressources/images/ecranDos.webp" alt="écran de dos" />
                    <p class = "description">Habere non (sed esse unus beate non implicari non pro non quas quos
                     amicitias beate non rerum non Graecia implicari beate esse sibi securitatem vivendum quos esse
                      si beate vivendum nimis ne esse cuique pro quos cuique habere pro adducas tamquam esse argutiis):
                       remittas non velis tamquam mirabilia sapientes cum.</p>
            
                </div>
                <!--<button class = "boutonDroite">droite -->
                
            </div>
            <button id="boutonD">D</button>
        </div>

        <section class = "sectionProduit">
            <p>
                Habere non (sed esse unus beate non implicari non pro non quas quos amicitias beate non rerum non Graecia implicari beate esse sibi securitatem vivendum quos esse si beate vivendum nimis ne esse cuique pro quos cuique habere pro adducas tamquam esse argutiis): remittas non velis tamquam mirabilia sapientes cum.
            </p>
            <p>Caractéristiques techniques</p>
        </section>
        
    </main>

    <button class="bouton-up hidden" id="js-prosition-scroll">
        <img 
            src="./ressources/images/angle-up-solid.svg" 
            alt="un triangle aux trois côtés égaux"
            height="50px"
            width="50px" />
        </button>
        <?php
            include("./pages/footer.php");
        ?>
    <script src="./ressources/js/defileImage.js"></script>
    <script src="./ressources/js/header-menu.js"></script>
    <script src="./ressources/js/returnButtonScroll.js"></script>
</body>
</html>