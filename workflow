
WORKFLOW



- BRANCHE COMMUNE
	- 4 BRANCHES pour chaque version de header.php et footer.php et leurs css
		- une version finale parmis les 4 sera choisit
		- merge header.php et footer.php (version finale) et leurs css sur la branche master

- BRANCHE PAGES
	- BRANCHE PRODUIT
		- produit.php, produit.css
	- BRANCHE ACTU
		- actualite.php, actualite.css
	- BRANCHE EQUIPE
		- equipe.php, equipe.css
	- BRANCHE CONTACT
		- contact.php, contact.css

	- un dossier "media" contenant 3 sous-dossiers: "images", "icones", "ressources"

- BRANCHE MASTER
	- un fichier screeny.php
		- on "git merge" avec les branches commune et pages une fois que tout est fini
		- on intègre header.php et footer.php avec un <?php include("") ?>
		- on ajoute le code de chaque page à screeny.php


organisation des fichiers:

	- screeny
		- pages
			- contact.php
			- actu.php
			- equipe.php
			- header.php
			- footer.php

		- ressources
			- images
			- css
				- contact.css
				- actu.css
				- equipe.css
				- header.css
				- footer.css
				- index.css

		- index.php (page produit => page d'accueil)
